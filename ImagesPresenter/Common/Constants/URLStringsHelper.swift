//
//  URLStringsHelper.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

class URLStringsHelper {
    
    enum URLKey: String {
        case baseURL    = "https://api.unsplash.com"
        case photos     = "/photos"
    }
    
    static func string(for key: URLKey) -> String {
        switch key {
        default:
            return key.rawValue
        }
    }
    
}
