//
//  StringsHelper.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//


class StringsHelper {
    
    enum StringKey: String {
        
        case badRequestErrorDomain                  = "BadRequestErrorDomain"
        case badRequestErrorDescription             = "Bad request error"
        
        case notInternetConnectionErrorDomain       = "NotInternetConnectionErrorDomain"
        case notInternetConnectionErrorDescription  = "The request failed. Check your network connection."
        
        case serializationErrorDomain               = "SerializationErrorDomain"
        case serializationErrorDescription          = "Response serialization error."

        case initErrorDomain                        = "InitErrorDomain"
        case initErrorDescription                   = "Couldn't init object, check init description."
        
        case authorizationErrorDomain               = "AuthorizationErrorDomain"
        case notAuthorizedErrorDescription          = "Authorization Error"
        
        case unknownErrorDomain                     = "UnknownErrorDomain"
        case unknownErrorHasOccured                 = "Unknown error"

        case internalServerErrorDomain              = "InternalServerErrorDomain"
        case internalServerErrorDescription         = "Server error"
        
        case notFoundErrorDomain                    = "NotFoundErrorDomain"
        case notFoundErrorDescription               = "Not found"
        
    }
    
    
    static func string(for key: StringKey) -> String {
        switch key {
        default:
            return key.rawValue
        }
    }
}
