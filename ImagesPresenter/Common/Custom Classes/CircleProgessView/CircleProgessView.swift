//
//  CircleLoadProgess.swift
//  ImagesPresenter
//
//  Created by Rustam on 12/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

import UIKit

class CircleProgessView: UIView {
    
    var bgPath: UIBezierPath!
    var shapeLayer: CAShapeLayer!
    var progressLayer: CAShapeLayer!
    
    var progress: Float = 0 {
        willSet(newValue)
        {
            progressLayer.strokeEnd = CGFloat(newValue)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        bgPath = UIBezierPath()
        createShape()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        bgPath = UIBezierPath()
        createShape()
    }
    
    private func createShape() {
        
        createCirclePath()
        shapeLayer = CAShapeLayer()
        shapeLayer.path = bgPath.cgPath
        shapeLayer.lineWidth = 5
        shapeLayer.fillColor = nil
        shapeLayer.strokeColor = UIColor.white.cgColor 
        
        progressLayer = CAShapeLayer()
        progressLayer.path = bgPath.cgPath
        progressLayer.lineCap = CAShapeLayerLineCap.round
        progressLayer.lineWidth = 5
        progressLayer.fillColor = nil
        progressLayer.strokeColor = UIColor.lightGray.cgColor
        progressLayer.strokeEnd = 0.0
        
        
        self.layer.addSublayer(shapeLayer)
        self.layer.addSublayer(progressLayer)
    }
    
    private func createCirclePath() {

        let x = self.frame.width/2
        let y = self.frame.height/2
        let center = CGPoint(x: x + x/2, y: y + y/2)
        
        bgPath.addArc(withCenter: center, radius: x/CGFloat(2), startAngle: CGFloat(0), endAngle: CGFloat.pi * 2, clockwise: true)
        bgPath.close()
    }
    
    
}
