//
//  BaseServiceImplementation.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

class BaseServiceImplementation: BaseService {
    
    var baseURL: String = URLStringsHelper.string(for: .baseURL)
    var errorsFactory: ErrorsFactory = ErrorsFactoryImplementation()
    var authenticationManager: AuthenticationManager = AuthenticationManagerImplementation()
    

}
