//
//  BaseService.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

protocol BaseService {
    
    var baseURL: String { get set }
    var errorsFactory: ErrorsFactory { get set }
    var authenticationManager: AuthenticationManager { get set }
    
}
