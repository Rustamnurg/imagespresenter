//
//  PhotosListViewController.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

import UIKit

class PhotosListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl : UIRefreshControl!
    
    var mainService: MainService!
    var dataManager: DataManager!
    
    let limit = 10
    
    var photosArr = [Photo]()
    
    var photosLoadingTask: CancellableTask?
    var photosPaginationTask: CancellableTask?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    func initView() {
        
        mainService = MainServiceImplementation()
        dataManager = DataManagerImplementation.sharedInstance
        loadPhotos()
        
        photosArr = PhotoObject.transform(photoObjectArr: dataManager.get(with: PhotoObject.self, predicate: nil) ?? [])
        
        self.navigationItem.title = "Photos"
        
        tableView.register(UINib(nibName: "PhotoCell", bundle: nil), forCellReuseIdentifier: "PhotoCell")
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UIScreen.main.bounds.width - 16
        
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshControlAction), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
}


extension PhotosListViewController {
    //MARK: - action
    
    @objc func refreshControlAction() {
        loadPhotos()
    }
    
}


extension PhotosListViewController {
    //MARK: - work with newtwork
    
    func loadPhotos() {
        
        photosLoadingTask?.cancel()
        photosPaginationTask?.cancel()
        self.tableView.tableFooterView = nil
        
        photosLoadingTask = mainService.photos(limit: limit, offset: 1, completion: { [weak self] (result) in
            self?.refreshControl.endRefreshing()
            switch result {
            case .success(let photosArr):
                guard self != nil else { return }
                self!.photosArr = photosArr
                self!.reloadTable()
                self!.saveNewData(photosArr)
            case .failure(let error):
                print(error.localizedDescription)
            case .cancel:
                break
            }
        })
    }
    
    
    func loadPaginatePhotos(offset: Int, limit: Int) {
        
        photosPaginationTask?.cancel()
        photosPaginationTask = mainService.photos(limit: limit, offset: offset, completion: { [weak self] (result) in
            self?.tableView.tableFooterView = nil
            switch result {
            case .success(let photosArr):
                self?.photosArr.append(contentsOf: photosArr)
                self?.reloadTable()
            case .failure(let error):
                print(error.localizedDescription)
            case .cancel:
                break
            }
        })
    }
    
}


extension PhotosListViewController {
    //MARK: - work with data

    func saveNewData(_ photosArr: [Photo]) {
        dataManager.delete(with: PhotoObject.self, predicate: nil)
        _ = PhotoObject.transform(photosArr: photosArr)
        dataManager.saveAll()
    }
    
}


extension PhotosListViewController: UITableViewDelegate, UITableViewDataSource {
    //MARK: table view
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photosArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        cell.loadImageWithAnimation(photosArr[indexPath.row].urls?.regular ?? "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == photosArr.count - 1 {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            self.tableView.tableFooterView = spinner
            self.tableView.tableFooterView?.isHidden = false
            loadPaginatePhotos(offset: photosArr.count + 1, limit: limit)
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if photosArr.count > 0 {
            numOfSections = 1
            tableView.backgroundView = nil
        } else {
            let noDataLabel: UILabel    = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "No data available"
            noDataLabel.textColor = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        }
        return numOfSections
    }
    
    func reloadTable() {
        self.tableView.reloadData()
    }
    
    
}
