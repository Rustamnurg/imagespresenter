//
//  PhotoCell.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoCell: UITableViewCell {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var progressView: CircleProgessView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func prepareForReuse() {
        progressView.progress = 0
    }
    
    func loadImageWithAnimation(_ urlString: String){
        
        var isLoaded = false
        let url = URL(string: urlString)
        
        SDWebImageManager.shared().diskImageExists(for: url) { (res) in
            isLoaded = res
        }
        
        if !isLoaded {
            self.progressView.isHidden = false
            photoImageView.sd_setImage(with: url, placeholderImage: nil, options: SDWebImageOptions.highPriority, progress: {  (currentProgress, allProgress, _)  in
                
                if currentProgress > 0, allProgress > 0 {
        
                    DispatchQueue.main.async {
                        self.progressView.progress = Float(currentProgress)/Float(allProgress)
                    }
                }
                
            }) { (_, _, _, _) in
                self.progressView.isHidden = true
            }
        } else {
            progressView.isHidden = true
        }
        
    }
    
}
