//
//  MainService.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

enum MainServiceBaseResult {
    case success([Photo])
    case failure(Error)
    case cancel()
}

protocol MainService {
    
    func photos(limit: Int, offset: Int, completion: @escaping (MainServiceBaseResult) -> Void) -> CancellableTask?

}
