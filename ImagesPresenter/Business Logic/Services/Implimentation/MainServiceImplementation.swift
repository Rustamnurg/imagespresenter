//
//  MainServiceImplementation.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

import Alamofire
import Foundation

class MainServiceImplementation: BaseServiceImplementation, MainService {

    func photos(limit: Int, offset: Int, completion: @escaping (MainServiceBaseResult) -> Void) -> CancellableTask? {
        
        guard let clientID = authenticationManager.accessKey else {
            completion(.failure(self.errorsFactory.unauthorizedError))
            return nil
        }
        
        var params = [String: Any]()
        
        params["page"] = offset
        params["per_page"] = limit
        params["client_id"] = clientID

        let dataRequest = request(baseURL + URLStringsHelper.string(for: .photos), method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).response { (dataResponse) in
            
            var result: MainServiceBaseResult
            
            defer {
                completion(result)
            }
            
            
            if let error = dataResponse.error as NSError? {
                if error.code == NSURLErrorCancelled {
                    result = .cancel()
                    return
                }
            }
            
            
            guard let response = dataResponse.response else {
                result = .failure(self.errorsFactory.urlError(error: dataResponse.error))
                return
            }
            
            
            print(response.statusCode)
            
            switch response.statusCode {
            case 200, 201:
                
                guard let data = dataResponse.data else {
                    result = .failure(self.errorsFactory.serializationError)
                    return
                }
                
                do {
                    let photosArr: [Photo] = try JSONDecoder().decode(Array<Photo>.self, from: data)
                    result = .success(photosArr)
                } catch  {
                    result = .failure(self.errorsFactory.serializationError)
                }
            default:
                result = .failure(self.errorsFactory.errorForStatusCode(response.statusCode))
            }
        }
        return dataRequest.task
    }
}
