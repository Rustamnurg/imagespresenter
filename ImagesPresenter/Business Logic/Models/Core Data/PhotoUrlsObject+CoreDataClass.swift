//
//  PhotoUrlsObject+CoreDataClass.swift
//  ImagesPresenter
//
//  Created by Rustam on 12/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//
//

import Foundation
import CoreData

public class PhotoUrlsObject: NSManagedObject {
    
    /// Transfrom optional PhotoUrlsObject to optional PhotoUrls
    ///
    /// - Parameter photoUrlsObject: object to transfrom
    /// - Returns: transfrom result
    static func transform(photoUrlsObject: PhotoUrlsObject?) -> PhotoUrls? {
        guard let photoUrls = photoUrlsObject else { return nil }
        var urls = PhotoUrls()
        urls.full = photoUrls.full
        urls.raw = photoUrls.raw
        urls.regular = photoUrls.regular
        urls.small = photoUrls.small
        urls.thumb = photoUrls.thumb
        return urls
    }
    
    
    /// Transfrom optional PhotoUrls to optional PhotoUrlsObject
    ///
    /// - Parameter photoUrls: object to transfrom
    /// - Returns: transfrom result
    static func transform(photoUrls: PhotoUrls?) -> PhotoUrlsObject? {
        guard let photoUrls = photoUrls else { return nil }
        let urlsObject = PhotoUrlsObject(context: DataManagerImplementation.sharedInstance.getContext())
        urlsObject.full = photoUrls.full
        urlsObject.raw = photoUrls.raw
        urlsObject.regular = photoUrls.regular
        urlsObject.small = photoUrls.small
        urlsObject.thumb = photoUrls.thumb
        return urlsObject
    }
}
