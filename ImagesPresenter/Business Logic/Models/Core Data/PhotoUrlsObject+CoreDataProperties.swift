//
//  PhotoUrlsObject+CoreDataProperties.swift
//  ImagesPresenter
//
//  Created by Rustam on 12/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//
//

import Foundation
import CoreData


extension PhotoUrlsObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PhotoUrlsObject> {
        return NSFetchRequest<PhotoUrlsObject>(entityName: "PhotoUrlsObject")
    }

    @NSManaged public var raw: String?
    @NSManaged public var full: String?
    @NSManaged public var regular: String?
    @NSManaged public var small: String?
    @NSManaged public var thumb: String?

}
