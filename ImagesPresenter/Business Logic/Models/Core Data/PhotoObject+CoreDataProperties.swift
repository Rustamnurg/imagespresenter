//
//  PhotoObject+CoreDataProperties.swift
//  ImagesPresenter
//
//  Created by Rustam on 12/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//
//

import Foundation
import CoreData


extension PhotoObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PhotoObject> {
        return NSFetchRequest<PhotoObject>(entityName: "PhotoObject")
    }

    @NSManaged public var height: Double
    @NSManaged public var id: String?
    @NSManaged public var width: Double
    @NSManaged public var urls: PhotoUrlsObject?

}
