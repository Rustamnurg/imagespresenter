//
//  DataManager.swift
//  ImagesPresenter
//
//  Created by Rustam on 12/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

import CoreData

protocol DataManager {
    
    /// Remove object from core data
    ///
    /// - Parameter managedObject: object type
    func delete<T>(managedObject: T) where T: NSManagedObject
    
    
    /// Get array of object filtred with predicate
    ///
    /// - Parameters:
    ///   - type: object type
    ///   - predicate: request predicate
    /// - Returns: result arr
    func get<T: NSManagedObject>(with type: T.Type, predicate: NSPredicate?) -> [T]?
    
    
    /// Save all chainges on core data
    func saveAll()
    
    
    /// Get current context
    ///
    /// - Returns: NSManagedObjectContext
    func getContext()-> NSManagedObjectContext
    
    
    /// Remove objects from core data
    ///
    /// - Parameters:
    ///   - type: object type
    ///   - predicate: request predicate
    func delete<T>(with type: T.Type, predicate: NSPredicate?) where T: NSManagedObject
    
    
}
