//
//  PhotoObject+CoreDataClass.swift
//  ImagesPresenter
//
//  Created by Rustam on 12/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//
//

import Foundation
import CoreData


public class PhotoObject: NSManagedObject {
    
    /// Transfrom from PhotoObject arr to Photo arr
    ///
    /// - Parameter photoObjectArr: array to transfrom
    /// - Returns: transform result
    static func transform(photoObjectArr: [PhotoObject]) -> [Photo] {
        guard photoObjectArr.count > 0 else { return [] }
        var result = [Photo]()
        for (_, item) in photoObjectArr.enumerated() {
            var photo = Photo()
            photo.height = item.height
            photo.width = item.width
            photo.id = item.id
            photo.urls = PhotoUrlsObject.transform(photoUrlsObject: item.urls)
            result.append(photo)
        }
        return result
    }
    
    
    /// Transfrom from Photo arr to PhotoObject arr
    ///
    /// - Parameter photosArr: array to transfrom
    /// - Returns: transform result
    static func transform(photosArr: [Photo]) -> [PhotoObject] {
        guard photosArr.count > 0 else { return [] }
        var result = [PhotoObject]()
        for (_, item) in photosArr.enumerated() {
            let photoObject = PhotoObject(context: DataManagerImplementation.sharedInstance.getContext())
            photoObject.height = item.height ?? 0
            photoObject.width = item.width ?? 0
            photoObject.id = item.id
            photoObject.urls = PhotoUrlsObject.transform(photoUrls: item.urls)
            result.append(photoObject)
        }
        return result
    }
    
}
