//
//  PhotoObject.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

struct Photo: Codable {
    
    var id      : String?
    var width   : Double?
    var height  : Double?
    var urls    : PhotoUrls?
}
