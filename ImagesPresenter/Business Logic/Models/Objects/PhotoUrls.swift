//
//  PhotoUrlsObject.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

import UIKit

struct PhotoUrls: Codable {
    
    var raw     : String?
    var full    : String?
    var regular : String?
    var small   : String?
    var thumb   : String?
}
