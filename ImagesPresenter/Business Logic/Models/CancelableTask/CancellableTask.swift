//
//  CancellableTask.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

enum TaskState {
    case running
    case suspended
    case canceling
    case completed
}

protocol CancellableTask {
    
    func cancel()
    
    var taskState: TaskState { get }
}
