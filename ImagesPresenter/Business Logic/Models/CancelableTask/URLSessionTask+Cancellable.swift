//
//  URLSessionTask+Cancellable.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

import Foundation

extension URLSessionTask: CancellableTask {
    
    var taskState: TaskState {
        switch state {
        case .running:
            return .running
        case .canceling:
            return .canceling
        case .completed:
            return .completed
        case .suspended:
            return .suspended
        }
    }
}
