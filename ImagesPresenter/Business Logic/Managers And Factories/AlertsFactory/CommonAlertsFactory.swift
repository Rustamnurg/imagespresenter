//
//  CommonAlertsFactory.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

import UIKit

protocol CommonAlertsFactory {

    func getOKAlert(withTitle title: String?, text: String?, completionBlock: @escaping () -> Void?) -> UIViewController

}
