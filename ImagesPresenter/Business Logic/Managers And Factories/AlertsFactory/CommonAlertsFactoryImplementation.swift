//
//  CommonAlertsFactoryImplementation.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

import UIKit

class CommonAlertsFactoryImplementation: CommonAlertsFactory {
    
    func getOKAlert(withTitle title: String?, text: String?, completionBlock: @escaping () -> Void?) -> UIViewController {
        
        let alertController = UIAlertController(title: title, message: text, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .cancel) { (_) in
            completionBlock()
        }
        alertController.addAction(okAction)
        
        return alertController
    }    
    
}
