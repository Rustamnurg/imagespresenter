//
//  ErrorsFactory.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

protocol ErrorsFactory {
    
    var serializationError: Error { get }
    var badRequestError: Error { get }
    var unauthorizedError: Error { get }
    var unknownError: Error { get }
    var internalServerError: Error { get }
    var notFoundError: Error { get }
    var connectionError: Error { get }
    
    func urlError(error: Error?) -> Error
    func errorForStatusCode(_ statusCode: Int) -> Error
    
}
