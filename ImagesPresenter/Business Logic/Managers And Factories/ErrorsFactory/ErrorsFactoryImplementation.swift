
//
//  ErrorsFactoryImplementation.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

import Foundation

class ErrorsFactoryImplementation: ErrorsFactory {
    
    var serializationError: Error {
        get {
            let error = NSError(domain: StringsHelper.string(for: .serializationErrorDomain), code: -1, userInfo: [NSLocalizedDescriptionKey : StringsHelper.string(for: .serializationErrorDescription)])
            return error
        }
    }
    
    var badRequestError: Error {
        get {
            let error = NSError(domain: StringsHelper.string(for: .badRequestErrorDomain), code: 400, userInfo: [NSLocalizedDescriptionKey : StringsHelper.string(for: .badRequestErrorDescription)])
            return error
        }
    }
    
    var unauthorizedError: Error {
        get {
            let error = NSError(domain: StringsHelper.string(for: .authorizationErrorDomain), code: 401, userInfo: [NSLocalizedDescriptionKey : StringsHelper.string(for: .notAuthorizedErrorDescription)])
            return error
        }
    }
    
    var unknownError: Error {
        get {
            let error = NSError(domain: StringsHelper.string(for: .unknownErrorDomain), code: -1, userInfo: [NSLocalizedDescriptionKey : StringsHelper.string(for: .unknownErrorHasOccured)])
            return error
        }
    }
    
    var internalServerError: Error {
        get {
            let error = NSError(domain: StringsHelper.string(for: .internalServerErrorDomain), code: 500, userInfo: [NSLocalizedDescriptionKey : StringsHelper.string(for: .internalServerErrorDescription)])
            return error
        }
    }
    
    var notFoundError: Error {
        get {
            let error = NSError(domain: StringsHelper.string(for: .notFoundErrorDomain), code: -1, userInfo: [NSLocalizedDescriptionKey : StringsHelper.string(for: .notFoundErrorDescription)])
            return error
        }
    }
    
    var connectionError: Error {
        get {
            let error = NSError(domain: StringsHelper.string(for: .notInternetConnectionErrorDomain), code: -1009, userInfo: [NSLocalizedDescriptionKey : StringsHelper.string(for: .notInternetConnectionErrorDescription)])
            return error
        }
    }
    
    
    func errorForStatusCode(_ statusCode: Int) -> Error {
        switch statusCode {
        case 400:
            return badRequestError
        case 401:
            return unauthorizedError
        case 404:
            return notFoundError
        case 500:
            return internalServerError
        default:
            return unknownError
        }
    }
    
    func urlError(error: Error?) -> Error {
        guard let urlError = error as? URLError else {
            return self.unknownError
        }
        switch urlError.code {
        case .timedOut, .networkConnectionLost, .notConnectedToInternet:
            return self.connectionError
        default:
            return self.unknownError
        }
        
        
    }
}
