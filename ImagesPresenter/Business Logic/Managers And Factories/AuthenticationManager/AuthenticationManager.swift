//
//  AuthenticationManager.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

protocol AuthenticationManager {
    
    var accessKey: String? { get }
    
    var secretKey: String? { get }
}
