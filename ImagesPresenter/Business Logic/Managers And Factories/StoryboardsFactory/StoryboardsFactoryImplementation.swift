//
//  StoryboardsFactoryImplementation.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

import UIKit

class StoryboardsFactoryImplementation: StoryboardsFactory {
    
    func getStoryboard(with name: StoryboardName) -> UIStoryboard {
        return UIStoryboard(name: name.rawValue, bundle: nil)
    }
}
