//
//  StoryboardsFactory.swift
//  ImagesPresenter
//
//  Created by Rustam on 11/12/2018.
//  Copyright © 2018 Rustam. All rights reserved.
//

import UIKit

protocol StoryboardsFactory {
    func getStoryboard(with name: StoryboardName) -> UIStoryboard
}

enum StoryboardName: String {
    case main         = "Main"
}

enum ModuleName: String {
    case photosList = "PhotosList"
}

extension UIStoryboard {
    
    /// Instantiates module view controller
    ///
    /// - Parameter moduleName: module name to get needed controller
    /// - Returns: uiviewcontroller of module
    func instantiateViewController(with moduleName: ModuleName) -> UIViewController {
        return instantiateViewController(withIdentifier: moduleName.rawValue)
    }
    
}
